DESCRIPTION
------------
This module provides countries taxonomy list.

REQUIREMENTS
------------
Drupal 8.x

INSTALLATION
------------
1.  Place countries_taxonomy module into your modules directory.
    This is normally the "modules" directory.

2.  Go to admin/modules. Enable modules.
    The Countries Taxonomy module is found in the Taxonomy section.

REFERENCES
----------
http://en.wikipedia.org/wiki/List_of_countries
